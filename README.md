# À propos des projets incubés dans le cadre de France Relance 🇫🇷

Les projets ici listés sont portés par les lauréats de la consultation France Relance 2021.

Ils ont vocation à être utiles pour des représentants de territoires.

Tous les projets sont libres d'utilisation et leur déploiement est testé par l'équipe de l'Incubateur des territoires.

L'Incubateur des territoire accompagne une grande partie de ces projets, notamment sur le plan technique.

Chaque dépôt contient des sources qui sont soit directement gérées par son porteur de projet, soit mirorées depuis un dépôt externe où le porteur publie ses sources et sa documentation.

Les projets sont mis à jour régulièrement ou automatiquement pendant toute la période d'incubation (2022-2023).
